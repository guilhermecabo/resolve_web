import axios from 'axios';

export const baseURL = 'https://api-resolveja.herokuapp.com/';

const api = axios.create({
  baseURL,
})

export default api
