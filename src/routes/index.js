import React from "react";
import { ToastContainer } from "react-toastify";
import { Switch } from "react-router-dom";
import Route from "./Route";
import SignIn from "~/pages/SignIn";
import SignUp from "~/pages/SignUp";
import Dashboard from "~/pages/Dashboard";
import Profile from "~/pages/Profile";
import Attendance from "~/pages/Attendance";
import Services from "~/pages/Services";
import ServiceForm from "../pages/ServiceForm";

export default function Routes() {
  return (
    <Switch>
      <Route path="/login" exact component={SignIn} />
      <Route path="/register" exact component={SignUp} />
      <Route path="/" exact component={Dashboard} isPrivate />
      <Route path="/dashboard" exact component={Dashboard} isPrivate />
      <Route path="/attendance/:id" exact component={Attendance} isPrivate />
      <Route path="/profile" exact component={Profile} isPrivate />
      <Route path="/services" exact component={Services} isPrivate />
      <Route
        path="/service/form/:id?"
        exact
        component={ServiceForm}
        isPrivate
      />

      <Route path="/" component={() => <h1>404</h1>} />
    </Switch>
  );
}
