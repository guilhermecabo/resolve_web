import React, { useRef, useEffect } from "react";
import CurrencyInput from "react-currency-masked-input";
import { useField } from "@rocketseat/unform";

export default function CurrencyMaskedInput({ name, placeholder }) {
  const ref = useRef(null);
  
  const { fieldName, registerField } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: ref.current,
      path: "state.value"
    });
  }, [ref.current, fieldName]); // eslint-disable-line

  return (
    <CurrencyInput
      name={fieldName}
      ref={ref}
      placeholder={placeholder}
      separator="."
    />
  );
}
