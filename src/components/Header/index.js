import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import Notifications from "~/components/Notifications";

import logo from "~/assets/images/logo.png";

import { Container, Content, Profile } from "./styles";
import { baseURL } from "~/services/api";

export default function Header() {
  const profile = useSelector(state => state.user.profile);
  const image = `${baseURL}image/provider/${useSelector(
    state => state.user.profile.image
  )}/`;

  

  return (
    <Container>
      <Content>
        <nav>
          <img className="logo-nav" src={logo} alt="Resolve" />
          <Link to="/dashboard">DASHBOARD</Link>
          <Link to="/services">SERVIÇOS</Link>
        </nav>

        <aside>
          <Notifications />
          <Profile>
            <div>
              <strong>{profile.username}</strong>
              <Link to="/profile">Meu perfil</Link>
            </div>
            <img
              src={ !!profile.image ?
                image :
                "https://api.adorable.io/avatars/50/abott@adorable.png/"
              }
              alt="Guilherme Cabo"
            />
          </Profile>
        </aside>
      </Content>
    </Container>
  );
}
