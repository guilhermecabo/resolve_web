import React, { useRef, useEffect } from "react";
import MaskedInput from 'react-input-mask';
import { useField } from "@rocketseat/unform";

export default function CurrencyMaskedInput({ name, ...rest }) {
  const ref = useRef(null);
  
  const { fieldName, registerField } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: ref.current,
      path: "state.value"
    });
  }, [ref.current, fieldName]); // eslint-disable-line

  return (
    <MaskedInput
      name={fieldName}
      ref={ref}
      {...rest}
    />
  );
}
