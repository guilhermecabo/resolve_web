import styled from 'styled-components'

export const Box = styled.div`
  border-radius: 1%;
  background: ${props => props.background || `white`};
  padding: 20px;
  display: flex;
  flex-direction: column;
  width: 30vw;
  height: 80vh;
`
export const BodyWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`
export const CustomInput = styled.input`
  height: 7vh;
  border: none;
  border-radius: 8px;
  background: rgba(0, 0, 0, 0.1);
  padding: 15px;
  margin-top: 0;
  margin-bottom: 2vh;
  outline: none;
  font-size: 20px;
  color: rgb(255, 255 , 255);
`

export const CustomButton = styled.button`
  border: none;
  background: white;
  display: flex;
  justify-content: center;
  padding: 15px;
  align-items: center;
  outline: none;
  border-radius: 4px;
  color: #c87137;
  cursor: pointer;
  width: 100%;
  min-width: 100px;
  font-size: 2.5vh;
  font-weight: 600;
  transition: 0.1s;
`