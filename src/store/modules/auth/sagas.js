import { takeLatest, call, put, all } from "redux-saga/effects";
import { toast } from "react-toastify";

import api from "../../../services/api";
import history from "../../../services/history";

import { signInSuccess, signFailure, signUpSuccess } from "./actions";

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;

    const response = yield call(api.post, "login/provider/", {
      email,
      password
    });

    const { user } = response.data;
    const { token } = response.data.token;

    yield put(signInSuccess(token, user));

    window.location.href = "/dashboard";
  } catch (err) {
    toast.error("Falha na autenticação, verifique seus dados");
    yield put(signFailure());
  }
}

export function* signUp({ payload }) {
  const {
    username,
    email,
    password,
    cpf_cnpj,
    uf,
    cidade,
    bairro,
    rua,
    numero,
    type,
    category_id
  } = payload;

  try {
    yield call(api.post, "providers", {
      username,
      email,
      password,
      cpf_cnpj,
      uf,
      cidade,
      bairro,
      rua,
      numero,
      type,
      category_id,
      rating: 5
    });

    yield put(signUpSuccess());

    history.push("/login");
  } catch (err) {
    //toast.error("Falha no cadastro, verifique seus dados!");
    toast.error(`Error: ${err.message} / ${err.status}`);
    yield put(signFailure());
  }
}

export function setToken({ payload }) {
  if (!payload) return;

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export function signOut() {
  history.push("/login");
}

export default all([
  takeLatest("persist/REHYDRATE", setToken),
  takeLatest("@auth/SIGN_IN_REQUEST", signIn),
  takeLatest("@auth/SIGN_UP_REQUEST", signUp),
  takeLatest("@auth/SIGN_OUT", signOut)
]);
