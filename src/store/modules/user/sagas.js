import { all, takeLatest, call, put } from "redux-saga/effects";
import { toast } from "react-toastify";

import api from "~/services/api";
import { updateProfileSuccess } from "./actions";

export function* updateProfile({ payload }) {
  try {
    const {
      username,
      email,
      cpf_cnpj,
      uf,
      cidade,
      bairro,
      rua,
      numero,
      image,
      ...rest
    } = payload.data;

    const profile = Object.assign(
      { username, email, cpf_cnpj, image },
      rest.oldPassword ? rest : {}
    );

    const response = yield call(api.put, "providers", profile);

    toast.success("Perfil atualizado com sucesso!");

    yield put(updateProfileSuccess(response.data));
  } catch (err) {
    toast.error(err.message);
    //toast.error('Erro ao atualizar perfil, verifique seus dados')
  }
}

export default all([takeLatest("@user/UPDATE_PROFILE_REQUEST", updateProfile)]);
