import React, { useState, useEffect } from "react";
import api from "~/services/api";
import history from "~/services/history";
import { Form, Input } from "@rocketseat/unform";

import CurrencyInput from "~/components/CurrencyMaskedInput";

import * as Yup from "yup";
import { toast } from "react-toastify";

import { Container } from "./styles";

export default function ServiceForm({ match }) {
  const [service, setService] = useState({});
  const [id, setId] = useState(match.params.id || null);

  useEffect(() => {
    async function handleService(id) {
      try {
        const { data } = await api.get(`service/${id}/`);
        setService(data.service);
      } catch (err) {
        console.log(err.message);
      }
    }

    handleService(id);
  }, []);

  const schema = Yup.object().shape({
    name: Yup.string().max(255, "O limite de caracteres é 255"),
    description: Yup.string().max(255, "O limite de caracteres é de 255"),
    avg_price: Yup.number().positive("Valor inválido")
  });

  async function handleSubmit(data) {
    if (id) {
      try {
        await api.put(`services/${id}`, data);
        toast.success("Serviço atualizado com sucesso");
        history.push("/services");
      } catch (err) {
        toast.error(err.message);
      }
    } else {
      try {
        data.avg_price = data.avg_price.toString().replace(",", ".");
        await api.post(`services`, data);
        toast.success("Serviço cadastrado com sucesso");
        history.push("/services");
      } catch (err) {
        toast.error(err.message);
      }
    }
  }

  return (
    <Container>
      <h1>Cadastrar novo serviço</h1>
      <Form initialData={service} schema={schema} onSubmit={handleSubmit}>
        <Input placeholder="Título do serviço" name="name" />
        <Input placeholder="Descrição" name="description" />
        <CurrencyInput name="avg_price" placeholder="Valor do serviço" />
        {/* <Input
          placeholder="Preço médio do serviço (Use ponto para separar reais de centavos)"
          type="number"
          step="0.01"
          min="0"
          name="avg_price"
        /> */}
        <button type="submit">Enviar</button>
      </Form>
    </Container>
  );
}
