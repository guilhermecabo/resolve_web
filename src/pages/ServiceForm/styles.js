import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  margin: 100px auto;
  max-width: 700px;

  h1 {
    color: #fff;
    text-align: center;
    margin-bottom: 30px;
  }

  form {
    display: flex;
    flex-direction: column;

    input,
    textarea {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;

      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }

      &[type="number"]::-webkit-inner-spin-button,
      &[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
    }

    textarea {
      line-height: 42px;
      resize: none;
      font-family: Roboto, sans-serif !important;
      font-size: 14px !important;

      &::placeholder {
        font-family: Roboto, sans-serif;
      }
    }

    button {
      margin: 5px 0 0;
      height: 44px;
      background: #ffffff;
      border: 0;
      border-radius: 4px;
      font-weight: bold;
      color: #dd6312f1;
      font-size: 16px;
      transition: 0.2s;

      &:hover {
        background: ${darken(0.05, "#ffffff")};
      }
      &:active {
        background: ${darken(0.15, "#ffffff")};
      }
    }
  }
`;
