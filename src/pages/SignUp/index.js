import React, { useState, useEffect } from "react";
import api from '~/services/api'
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Form, Input, Select } from "@rocketseat/unform";
import MaskedInput from '~/components/MaskedInput'
import * as Yup from "yup";

import logo from "~/assets/images/logo.png";

import { signUpRequest } from "~/store/modules/auth/actions";

const schema = Yup.object().shape({
  username: Yup.string().required("O nome é obrigatório"),

  email: Yup.string()
    .email("Insira um e-mail válido!")
    .required("O e-mail é obrigatório"),
  password: Yup.string()
    .min(6, "No mínimo 6 caracteres")
    .required("A senha é obrigatória"),
  cpf_cnpj: Yup.number("CPF inválido")
    .min(10000000000, "O tamanho mínimo do campo é de 11 caracteres")
    .max(99999999999999, "O tamanho máximo do campo é de 14 caracteres")
    .required("O CPF/CNPJ é obrigatório"),
  uf: Yup.string().required("A escolha de UF é obrigatória"),
  cidade: Yup.string().required("A cidade é obrigatória"),
  bairro: Yup.string().required("O bairro é obrigatório"),
  rua: Yup.string().required("A rua é obrigatória"),
  numero: Yup.string().required("O número do imóvel é obrigatório"),
  type: Yup.string().required("O tipo de prestador é obrigatório"),
  category_id: Yup.number("Categoria Inválida")
    .required("A categoria é obrigatória")
});

export default function SignUp() {
  const dispatch = useDispatch();

  const loading = useSelector(state => state.auth.loading);

  const [categories, setCategories] = useState([])

  useEffect(() => {
    async function getCategories() {
      const { data } = await api.get('categories')
      setCategories(data)
      console.log(data)
    }

    getCategories()
  })

  function handleSubmit({
    username,
    email,
    password,
    cpf_cnpj,
    uf,
    cidade,
    bairro,
    rua,
    numero,
    type,
    category_id
  }) {
    dispatch(
      signUpRequest(
        username,
        email,
        password,
        cpf_cnpj,
        uf,
        cidade,
        bairro,
        rua,
        numero,
        type,
        category_id
      )
    );
  }

  const uf = [
    { id: "AC", title: "AC" },
    { id: "AL", title: "AL" },
    { id: "AP", title: "AP" },
    { id: "AM", title: "AM" },
    { id: "BA", title: "BA" },
    { id: "CE", title: "CE" },
    { id: "DF", title: "DF" },
    { id: "ES", title: "ES" },
    { id: "GO", title: "GO" },
    { id: "MA", title: "MA" },
    { id: "MT", title: "MT" },
    { id: "MS", title: "MS" },
    { id: "MG", title: "MG" },
    { id: "PA", title: "PA" },
    { id: "PB", title: "PB" },
    { id: "PR", title: "PR" },
    { id: "PE", title: "PE" },
    { id: "PI", title: "PI" },
    { id: "RJ", title: "RJ" },
    { id: "RN", title: "RN" },
    { id: "RS", title: "RS" },
    { id: "RO", title: "RO" },
    { id: "RR", title: "RR" },
    { id: "SC", title: "SC" },
    { id: "SP", title: "SP" },
    { id: "SE", title: "SE" },
    { id: "TO", title: "TO" }
  ];

  const types = [
    { id: "Individual", title: "Individual" },
    { id: "Empresa", title: "Empresa" }
  ];

  const categoriesOptions = categories.map(categorie => {
      return { id: categorie.id, title: categorie.name }
  })

  return (
    <>
      <img className="logo" src={logo} alt="logo" />

      <h2>Preencha os dados para o cadastro</h2>

      <Form schema={schema} onSubmit={handleSubmit}>
        <Input
          name="username"
          type="text"
          id="username"
          placeholder="Seu nome ou nome da empresa"
        />

        <Input name="email" type="email" id="email" placeholder="Seu e-mail" />

        <Input
          name="password"
          placeholder="Sua senha"
          type="password"
          id="senha"
        />

        <Select
          placeholder="Selecione sua categoria de serviços"
          name="category_id"
          id="category_id"
          options={categoriesOptions}
        />

        <Select
          placeholder="Selecione seu tipo de prestador"
          name="type"
          id="type"
          options={types}
        />

        <Input
          name="cpf_cnpj"
          minLength="11"
          maxLength="14"
          type="number"
          id="cpf_cnpj"
          placeholder="CPF ou CNPJ (Somente números)"
        />

        <Select
          placeholder="Selecione seu estado"
          name="uf"
          id="uf"
          options={uf}
        />

        <Input name="cidade" type="text" id="cidade" placeholder="Sua cidade" />
        <Input name="bairro" type="text" id="bairro" placeholder="Seu bairro" />
        <Input name="rua" type="text" id="rua" placeholder="Sua rua" />
        <Input
          name="numero"
          type="text"
          id="numero"
          placeholder="Número do imóvel"
        />

        <button disabled={loading} type="submit">
          {loading ? "Carregando..." : "Cadastrar"}
        </button>

        <Link to="/login">Já possuo uma conta</Link>
      </Form>
    </>
  );
}
