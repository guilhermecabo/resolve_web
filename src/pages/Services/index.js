import React, { useState, useEffect } from "react";
import api from "~/services/api";
import history from "~/services/history";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { MdAdd } from "react-icons/md";

import { Container, Service } from "./styles";

export default function Services() {
  const [services, setServices] = useState([]);
  const profile = useSelector(state => state.user.profile);

  useEffect(() => {
    async function loadServices() {
      const { data } = await api.get(`services/${profile.id}`);
      setServices(data);
    }
    loadServices();
  }, []);

  function handleEdit(id) {
    history.push(`/service/form/${id}`);
  }

  return (
    <Container>
      <header>
        <strong>Serviços cadastrados</strong>
        <Link to="/service/form">
          <button>
            <MdAdd color="#FFF" size={40} />
          </button>
        </Link>
      </header>
      <main>
        <ul>
          {services.map(service => (
            <Service onClick={() => handleEdit(service.id)} key={service.id}>
              <div>
                <strong>{service.name}</strong>
                <span>{service.description}</span>
              </div>

              <div>
                <strong>{`~ R$ ${service.avg_price.toFixed(2).toString().replace(',', '.')}`}</strong>
              </div>
            </Service>
          ))}
        </ul>
      </main>
    </Container>
  );
}
