import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  max-width: 80vw;
  margin: 50px auto 0;
  padding-bottom: 50px;

  header {
    display: flex;
    justify-content: space-between;
    align-self: flex-start;
    align-items: center;
    padding: 15px 30px 15px 0;
    border-bottom: 2px solid rgba(255, 255, 255, 0.3);

    strong {
      color: #fff;
      font-size: 36px;
    }

    button {
      background: transparent;
      border: 0;
      transition: 0.2s;

      &:hover {
        opacity: 0.7;
      }
    }
  }

  main {
    width: 100%;

    ul {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      grid-gap: 15px;
      margin-top: 30px;
    }
  }
`;

export const Service = styled.li`
  height: 120px;
  display: flex;
  padding: 0 20px;
  justify-content: space-between;
  align-items: center;
  background: #fff;
  border-radius: 4px;
  cursor: pointer;

  &:hover {
    background: ${darken(0.05, "#FFF")};
  }

  div {
    height: 60px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;

    & + div {
      min-width: 150px;
    }

    strong {
      font-size: 26px;
      color: #dd6312f1;
    }

    span {
      font-size: 20px;
      color: rgba(0, 0, 0, 0.4);
    }
  }
`;
