import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  max-width: 80vw;
  margin: 50px auto 0;
  padding-bottom: 50px;

  display: flex;
  flex-direction: column;

  header {
    display: flex;
    align-self: flex-start;
    align-items: center;
    padding: 15px 30px 15px 0;
    border-bottom: 2px solid rgba(255, 255, 255, 0.3);

    strong {
      color: #fff;
      font-size: 36px;
    }
  }

  main {
    display: flex;
    align-self: flex-start;
    align-items: flex-start;
  }

  ul {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 15px;
    margin-top: 30px;
  }
`;

export const Attendance = styled.li`
  display: flex;
  justify-content: space-between;
  padding: 40px;
  border-radius: 4px;
  background: #fff;
  transition: 0.5s;
  cursor: pointer;

  &:hover {
    background: ${darken(0.1, "#fff")};
  }
`;

export const AttendanceDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-start;
  height: 50px;

  strong {
    color: #dd6312f1;
    font-size: 24px;
  }

  span {
    color: rgba(0, 0, 0, 0.6);
    font-size: 16px;
  }
`;

export const AttendanceStatus = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-end;
  height: 50px;
  border-left: 1px solid rgba(0, 0, 0, 0.2);
  padding-left: 20px;

  strong {
    color: rgba(0, 0, 0, 0.5);
    font-size: 14px;
  }

  span {
    color: rgba(0, 0, 0, 0.6);
    font-size: 12px;
  }

  time {
    color: rgba(0, 0, 0, 0.6);
  }
`;
