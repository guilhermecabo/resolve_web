import React, { useState, useEffect } from "react";
import api from "~/services/api";

import { Link } from "react-router-dom";

import { parseISO, formatDistance } from "date-fns";
import pt from "date-fns/locale/pt";

import {
  Container,
  Attendance,
  AttendanceDetails,
  AttendanceStatus
} from "./styles";

export default function Dashboard() {
  const [attendances, setAttendances] = useState([]);

  useEffect(() => {
    async function loadAttendances() {
      const response = await api.get("attendances/provider/");

      const data = response.data.map(attendance => {
        return {
          ...attendance,
          timeDistance: formatDistance(
            parseISO(attendance.created_at),
            new Date(),
            { addSuffix: true, locale: pt }
          ),
          link: `/attendance/${attendance.id}`
        };
      });

      setAttendances(data);
    }

    loadAttendances();
  }, [attendances]);

  return (
    <Container>
      <header>
        <strong>Serviços solicitados</strong>
      </header>
      <ul>
        {attendances.map(attendance => (
          <Link to={attendance.link} key={attendance.id}>
            <Attendance>
              <AttendanceDetails>
                <strong>{attendance.service.name}</strong>
                <span>Solicitado por {attendance.user.name}</span>
              </AttendanceDetails>
              <AttendanceStatus>
                <strong>{attendance.status}</strong>
                <time>{attendance.timeDistance}</time>
              </AttendanceStatus>
            </Attendance>
          </Link>
        ))}
      </ul>
    </Container>
  );
}
