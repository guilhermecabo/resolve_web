import React, { useState, useEffect } from "react";
import api from "~/services/api";

import { Container, Detail, Address, ButtonsContainer, Button } from "./styles";
import { toast } from "react-toastify";

export default function Attendance({ match }) {
  const [attendance, setAttendance] = useState({});
  const [loading, setLoading] = useState(true);
  const [updatingLoading, setUpdatingLoading] = useState(true);

  useEffect(() => {
    async function loadAttendance() {
      const { data } = await api.get(`attendance/provider/${match.params.id}/`);
      console.warn(data)
      setAttendance(data);
      setLoading(false);
      setUpdatingLoading(false);
    }
    loadAttendance();
  }, [attendance]);

  async function handleStatus(status) {
    setUpdatingLoading(true);
    const { data } = await api.put(`attendance/provider/${match.params.id}/`, {
      status
    });

    setAttendance({
      ...attendance,
      status: data.status,
      updated_at: data.updated_at
    });
    setUpdatingLoading(false);
    toast.success(`O serviço foi ${status.toLowerCase()}`);
  }

  return (
    <Container>
      {!loading && (
        <div>
          <h1>Solicitação de serviço</h1>

          <ul>
            <Detail>
              <strong>Serviço:</strong>
              <span>{attendance.service.name}</span>
            </Detail>
            <Detail>
              <strong>Nome do solicitante:</strong>
              <span>{attendance.user.name}</span>
            </Detail>
            <Detail>
              <strong>Email do solicitante:</strong>
              <span>{attendance.user.email}</span>
            </Detail>
            <Detail>
              <strong>Telefone do solicitante:</strong>
              <span>{attendance.user.tel || "Não informado"}</span>
            </Detail>
            <Detail>
              <strong>Forma de pagamento:</strong>
              <span>{`${attendance.attendance.payment_method} ${
                attendance.attendance.payment_method === "Dinheiro"
                  ? `(troco para R$ ${attendance.attendance.changeMoney.toFixed(2).toString().replace('.', ',')})`
                  : ""
              }`}</span>
            </Detail>
            <Detail>
              <strong>Horário escolhido:</strong>
              <span>{attendance.attendance.desiredTime}</span>
            </Detail>

            <h3>Endereço</h3>

            <Address>
              <strong>{attendance.attendance.endereco}</strong>
            </Address>
          </ul>

          {attendance.attendance.status === "Aguardando Confirmação" && (
            <ButtonsContainer>
              <Button
                disabled={updatingLoading}
                type="button"
                mode="refuse"
                onClick={() => handleStatus("Cancelado")}
              >
                Recusar
              </Button>
              <Button
                disabled={updatingLoading}
                type="button"
                mode="accept"
                onClick={() => handleStatus("Confirmado")}
              >
                Aceitar
              </Button>
            </ButtonsContainer>
          )}

          {attendance.attendance.status === "Confirmado" && (
            <ButtonsContainer>
              <Button
                disabled={updatingLoading}
                type="button"
                mode="refuse"
                onClick={() => handleStatus("Cancelado")}
              >
                Cancelar
              </Button>
              <Button
                disabled={updatingLoading}
                type="button"
                mode="accept"
                onClick={() => handleStatus("Finalizado")}
              >
                Finalizar
              </Button>
            </ButtonsContainer>
          )}
          {(attendance.attendance.status === "Finalizado" ||
            attendance.attendance.status === "Cancelado") && (
            <ButtonsContainer>
              <strong>O serviço foi {attendance.attendance.status.toLowerCase()}</strong>
            </ButtonsContainer>
          )}
        </div>
      )}
    </Container>
  );
}
