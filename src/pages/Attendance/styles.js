import styled, { css } from "styled-components";
import { darken, lighten } from "polished";

export const Container = styled.div`
  min-height: 80vh;
  max-width: 800px;
  margin: 50px auto 0;
  padding-bottom: 50px;

  & > div {
    width: 100%;
    height: 100%;
    padding-bottom: 20px;
    background: #fff;
    display: flex;
    flex-direction: column;
    border-radius: 4px;

    h3 {
      text-align: center;
      align-self: center;
      color: rgba(0, 0, 0, 0.4);
      font-size: 20px;
      margin: 60px 0 20px;
      padding-bottom: 10px;
      width: 30%;
      border-bottom: 1px solid rgba(0, 0, 0, 0.2);
    }
  }

  h1 {
    align-self: center;
    color: ${lighten(0.15, "#dd6312f1")};
    border-bottom: 1px solid rgba(0, 0, 0, 0.3);
    width: 50%;
    text-align: center;
    padding: 10px 0;
    margin-top: 20px;
  }

  ul {
    display: flex;
    flex-direction: column;
    height: 60%;
    width: 100%;
    padding: 50px 80px;
  }
`;

export const Detail = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;

  & + li {
    margin-top: 40px;
  }

  strong {
    font-size: 20px;
    color: ${lighten(0.15, "#dd6312f1")};
    margin-right: 10px;
  }

  span {
    font-size: 18px;
    color: rgba(0, 0, 0, 0.5);
  }
`;

export const Address = styled(Detail)`
  justify-content: center;
  margin-top: 10px;

  & + li {
    margin-top: 20px;
  }
`;

export const ButtonsContainer = styled.div`
  justify-self: flex-end;
  align-self: center;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 20% 0;
  justify-content: space-evenly;

  button {
    padding: 10px 20px;
    transition: 0.5s;
    border: 0;
    font-weight: bold;
    background: transparent;
    font-size: 20px;
    border-right: 0.5px solid rgba(0, 0, 0, 0.5);

    & + button {
      border-right: 0;
      border-left: 0.5px solid rgba(0, 0, 0, 0.5);
    }

    &:disabled {
      cursor: not-allowed;
    }
  }

  strong {
    color: rgba(0, 0, 0, 0.3);
    font-size: 18px;
  }
`;

export const Button = styled.button`
  ${props =>
    props.mode === "accept" &&
    css`
      & {
        color: rgb(89, 165, 89);
      }

      &:hover {
        color: ${darken(0.15, "rgb(89, 165, 89)")};
      }

      &:disabled {
        color: rgba(89, 165, 89, 0.3);
      }
    `}

  ${props =>
    props.mode === "refuse" &&
    css`
      & {
        color: rgb(240, 24, 24);
        font-size: 18px !important;
      }

      &:hover {
        color: ${darken(0.15, "rgb(240, 24, 24)")};
      }

      &:disabled {
        color: rgba(240, 24, 24, 0.3);
      }
    `}
`;
