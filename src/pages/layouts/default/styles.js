import styled from "styled-components";

export const Wrapper = styled.div`
  height: 100%;
  min-height: 100vh;
  width: 100%;
  min-width: 100vw;
  background: linear-gradient(-45deg, #ec975f 20%, #e8813c);
`;
