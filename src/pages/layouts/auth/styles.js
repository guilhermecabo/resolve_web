import styled from "styled-components";
import { darken } from "polished";

export const Wrapper = styled.div`
  height: 100%;
  min-height: 100vh;
  background: linear-gradient(90deg, #dd6312f1, #ec975f, #dd6312f1);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 405px;
  text-align: center;
  padding: 3% 0 4%;

  .logo {
    width: 12vw;
    min-width: 50px;
    height: 12vw;
    min-height: 50px;
    margin-bottom: 5vh;
  }

  h2 {
    color: white;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 30px;

    input {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;

      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }

      &[type="number"]::-webkit-inner-spin-button,
      &[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
    }

    span {
      color: #f2023b;
      align-self: flex-start;
      margin: 0 0 10px;
      font-weight: bold;
    }

    button {
      margin: 5px 0 0;
      height: 44px;
      background: #ffffff;
      border: 0;
      border-radius: 4px;
      font-weight: bold;
      color: #dd6312f1;
      font-size: 16px;
      transition: 0.2s;

      &:hover {
        background: ${darken(0.05, "#ffffff")};
      }
      &:active {
        background: ${darken(0.15, "#ffffff")};
      }
    }

    select {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;
      cursor: pointer;

      option {
        font-size: 20px;
        font-weight: 600;
        color: #dd6312f1;
        border-bottom: 1px solid white;
      }
    }

    a {
      color: #fff;
      margin-top: 15px;
      font-size: 16px;
      opacity: 0.8;
      transition: 0.1s;

      &:hover {
        opacity: 1;
      }
    }
  }
`;
