import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Form, Input } from "@rocketseat/unform";
import * as Yup from "yup";

import { signInRequest } from "~/store/modules/auth/actions";

import logo from "~/assets/images/logo.svg";

const schema = Yup.object().shape({
  email: Yup.string()
    .email("Insira um e-mail válido!")
    .required("O e-mail é obrigatório"),

  password: Yup.string().required("A senha é obrigatória")
});

export default function SignIn() {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.loading);

  function handleSubmit({ email, password }) {
    dispatch(signInRequest(email, password));
  }

  return (
    <>
      <img className="logo" src={logo} alt="Resolve" />

      <h2>Login</h2>

      <Form schema={schema} onSubmit={handleSubmit}>
        <Input name="email" type="email" id="email" placeholder="Seu e-mail" />

        <Input
          name="password"
          placeholder="Sua senha"
          type="password"
          id="senha"
        />

        <button disabled={loading} type="submit">
          {loading ? "Carregando..." : "Entrar"}
        </button>

        <Link to="/register">Criar minha conta gratuitamente</Link>
      </Form>
    </>
  );
}
