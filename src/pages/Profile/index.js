import React from "react";
import { Form, Input, Select } from "@rocketseat/unform";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";

import { signOut } from "~/store/modules/auth/actions";
import { updateProfileRequest } from "~/store/modules/user/actions";

import AvatarInput from "./AvatarInput";

import { Container } from "./styles";

export default function Profile() {
  const dispatch = useDispatch();
  const profile = useSelector(state => state.user.profile);
  const loading = useSelector(state => state.loading);

  function handleSubmit(data) {
    dispatch(updateProfileRequest(data));
  }

  function handleSignOut() {
    dispatch(signOut());
  }

  const uf = [
    { id: "AC", title: "AC" },
    { id: "AL", title: "AL" },
    { id: "AP", title: "AP" },
    { id: "AM", title: "AM" },
    { id: "BA", title: "BA" },
    { id: "CE", title: "CE" },
    { id: "DF", title: "DF" },
    { id: "ES", title: "ES" },
    { id: "GO", title: "GO" },
    { id: "MA", title: "MA" },
    { id: "MT", title: "MT" },
    { id: "MS", title: "MS" },
    { id: "MG", title: "MG" },
    { id: "PA", title: "PA" },
    { id: "PB", title: "PB" },
    { id: "PR", title: "PR" },
    { id: "PE", title: "PE" },
    { id: "PI", title: "PI" },
    { id: "RJ", title: "RJ" },
    { id: "RN", title: "RN" },
    { id: "RS", title: "RS" },
    { id: "RO", title: "RO" },
    { id: "RR", title: "RR" },
    { id: "SC", title: "SC" },
    { id: "SP", title: "SP" },
    { id: "SE", title: "SE" },
    { id: "TO", title: "TO" }
  ];

  const schema = Yup.object().shape({
    username: Yup.string().required("O nome é obrigatório"),
    email: Yup.string()
      .email("Insira um e-mail válido!")
      .required("O e-mail é obrigatório"),
    oldPassword: Yup.string().notRequired(),
    password: Yup.string().notRequired(),
    confirmPassword: Yup.string().notRequired(),
    cpf_cnpj: Yup.number("CPF inválido")
      .min(10000000000, "O tamanho mínimo do campo é de 11 caracteres")
      .max(99999999999999, "O tamanho máximo do campo é de 14 caracteres")
      .required("O CPF/CNPJ é obrigatório"),
    uf: Yup.string().required("A escolha de UF é obrigatória"),
    cidade: Yup.string().required("A cidade é obrigatória"),
    bairro: Yup.string().required("O bairro é obrigatório"),
    rua: Yup.string().required("A rua é obrigatória"),
    numero: Yup.string().required("O número do imóvel é obrigatório")
  });

  return (
    <Container>
      <Form initialData={profile} onSubmit={handleSubmit}>
        <AvatarInput name="image" />

        <Input name="username" placeholder="Nome de apresentação" />
        <Input name="email" type="email" placeholder="E-mail" />

        <Select
          placeholder="Selecione seu estado"
          name="uf"
          id="uf"
          options={uf}
        />
        <Input name="cidade" type="text" placeholder="Cidade" />
        <Input name="bairro" type="text" placeholder="Bairro" />
        <Input name="rua" type="text" placeholder="Rua" />
        <Input name="numero" type="text" placeholder="Número do imóvel" />
        <Input type="number" placeholder="CPF/CNPJ" name="cpf_cnpj" />

        <hr />

        <Input name="oldPassword" type="password" placeholder="Senha atual" />
        <Input name="password" type="password" placeholder="Nova senha" />
        <Input
          name="confirmPassword"
          type="password"
          placeholder="Confirmar nova senha"
        />

        <button disabled={loading} type="submit">
          {loading ? "Carregando..." : "Atualizar perfil"}
        </button>
      </Form>

      <button type="button" onClick={handleSignOut}>
        {loading ? "Carregando..." : "Sair"}
      </button>
    </Container>
  );
}
