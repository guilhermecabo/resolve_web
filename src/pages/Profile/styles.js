import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  max-width: 600px;
  margin: 50px auto 0;
  padding-bottom: 50px;

  h1 {
    color: white;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 30px;

    input {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;

      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }

      &[type="number"]::-webkit-inner-spin-button,
      &[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
    }

    span {
      color: #f2023b;
      align-self: flex-start;
      margin: 0 0 10px;
      font-weight: bold;
    }

    hr {
      border: 0;
      height: 1px;
      background: rgba(255, 255, 255, 0.2);
      margin: 10px 0 20px;
    }

    button {
      margin: 5px 0 0;
      height: 44px;
      background: #ffffff;
      border: 0;
      border-radius: 4px;
      font-weight: bold;
      color: #dd6312f1;
      font-size: 16px;
      transition: 0.2s;

      &:hover {
        background: ${darken(0.05, "#ffffff")};
      }
      &:active {
        background: ${darken(0.15, "#ffffff")};
      }
    }

    select {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;
      cursor: pointer;

      option {
        font-size: 20px;
        font-weight: 600;
        color: #dd6312f1;
        border-bottom: 1px solid white;
      }
    }
  }

  > button {
    width: 100%;
    margin: 10px 0 0;
    height: 44px;
    background: #ed091d;
    border: 0;
    border-radius: 4px;
    font-weight: bold;
    color: #fff;
    font-size: 16px;
    transition: 0.2s;

    &:hover {
      background: ${darken(0.05, "#ed091d")};
    }
    &:active {
      background: ${darken(0.15, "#ed091d")};
    }
  }
`;
