import React, { useState, useRef, useEffect } from "react";
import { useField } from "@rocketseat/unform";
import api, { baseURL } from "~/services/api";

import { Container } from "./styles";

export default function AvatarInput() {
  const { registerField } = useField("image");
  const defaultValue = `${baseURL}image/provider/${useField("image").defaultValue}/`

  const [file, setFile] = useState(defaultValue);
  const [preview, setPreview] = useState(defaultValue);
  const [image, setImage] = useState(null)

  const ref = useRef();

  useEffect(() => {
    if (ref.current) {
      registerField({
        name: "image",
        ref: ref.current,
        path: "dataset.file"
      });
    }

    console.log(file)
  }, [ref, registerField]);

  async function handleChange(e) {
    const data = new FormData();

    data.append("image", e.target.files[0]);

    const response = await api.post("image/provider", data);
    
    const { image } = response.data;

    const url = `${baseURL}image/provider/${image}`;

    setFile(image);
    setPreview(url);
  }

  return (
    <Container>
      <label htmlFor="avatar">
        <img
          src={
            file !== `${baseURL}image/provider/null/` ? preview : "https://api.adorable.io/avatars/50/abott@adorable.png"
          }
          alt=""
        />

        <input
          type="file"
          id="avatar"
          accept="image/*"
          data-file={file}
          onChange={handleChange}
        />
      </label>
    </Container>
  );
}
